
context('CSGO Tests', () => {
    beforeEach(() => {
      cy.visit('https://www.csgoroll.com/en/dice')
      
    })
  
    describe('Buttons Assertions on desktop res', () => {        
        const buttons = [
            {'label':'+1'},
            {'label':'+10'},
            {'label':'1/2'},
            {'label':'X2'}
        ]
        buttons.forEach((button) =>{
            it(`Bet ${button.label} button work as expected`, () => {
                cy.viewport(1920, 500) // Desktop HD                
                cy.get('#mat-input-4')
                    .invoke('val').then(value => {
                        cy.get('button')
                            .contains(button.label)
                            .click()
                        cy.get('#mat-input-4')
                            .invoke('val')
                                .then(valueUpdated => {
                                    //case +1, +10
                                    if(button.label.includes('+')){
                                        expect(Number(valueUpdated), 'using buttons works for addition').to.be.eq(Number(value) + Number(button.label.replace('+','')))
                                        cy.get('.currency-value')
                                        .invoke('text')
                                            .then(currentValue => {
                                                expect(Number(currentValue)).to.be.eq(Number(value) + Number(button.label.replace('+','')))
                                            })
                                    }
                                    //case 1/2
                                    if(button.label.includes('/')){
                                        expect(Number(valueUpdated)).to.be.eq(value / 2)
                                    } 
                                    //case X2, Xnumber       
                                    if(button.label.includes('X')){
                                        expect(Number(valueUpdated)).to.be.eq(value * button.label.replace('X',''))
                                    }             
                                })
                    })
              })
        })     
    })

    describe('Testing Inputs behaviors', () => {
       
        it(`Verify that the bet amount input updates other inputs`, () => {
            cy.scrollTo(0, 0)
            let betAmount= 200
            cy.get('#mat-input-4')
                .clear()
                .type(betAmount)
            cy.get('.currency-value')
            .invoke('text')
                .then(currentValue => {
                    expect(Number(currentValue)).to.be.eq(betAmount)
                })               
        })
        it(`If the bet amout its 0 then a error message shows up`, () => {
            cy.scrollTo(0, 0)
            let betAmount= 0
            cy.get('#mat-input-4')
                .clear()
                .type(betAmount)
            cy.get('.ng-star-inserted').contains('You must enter a greater value')         
        })
    })

    describe('Testing behavior of Dragging bar', () => {
        it(`Dragging to the right increase bet amout`, () => {
            cy.scrollTo(0, 0)
            cy.get('.handle')  
                .trigger('mousedown', { which: 1, pageX: 500, pageY: 0 })
                .trigger('mousemove', { which: 1, pageX: 700, pageY: 0 })
                .trigger('mouseup')  
                cy.log('⚠️ introduce your accertions here - pending')  
        })
        it(`Dragging to the left reduce amout`, () => {
            cy.scrollTo(0, 0)
            cy.get('.handle')  
                .trigger('mousedown', { which: 1, pageX: 500, pageY: 0 })
                .trigger('mousemove', { which: 1, pageX: 100, pageY: 0 })
                .trigger('mouseup')    
                cy.log('⚠️ introduce your accertions here - pending')
        })
    })
  
  })
  